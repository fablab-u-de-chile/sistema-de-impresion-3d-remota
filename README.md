# Sistema de impresión 3D remota

Este repositorio es para documentar el proceso de instalación y resolución de problemas del sistema de impresión 3D remoto del FabLab U. de Chile.

Este sistema esta basado en [OctoPrint](https://octoprint.org/) una interfaz web para impresoras 3D, de código abierto y gratuito. OctoPrint tiene una interfaz básica que sirve para controlar y monitorear tu impresora 3D, además puedes instalar diferentes PlugIns que permiten expandir sus funcionalidades. OctoPrint se instala en una Raspberry Pi que al conectarse a la impresora 3D mediante cable USB entrega el control de toda la máquina.

Para conectarse al servidor de impresión 3D se pueden utilizar los software de acceso remoto [remote.it](https://remote.it/) o [ngrok](https://ngrok.com) permiten acceder a la Raspberry Pi desde cualquier parte del mundo.

El sistema completo cuenta con 4 impresoras Ender 3, conectadas cada una a su propio servidor de impresión remota.

## Componentes

* Raspbery Pi (versiones recomendadas: 3B, 3B+ o 4B)
* Cámara Raspberry Pi
* Cable Cámara 60cm
* Cable USB (micro o mini según el modelo de la impresora)
* Fuente poder 5V 3A para la Raspberry
* Tarjeta micro SD de 16 o 32 gb
* Impresora 3D Creality Ender 3

## Instalación

Para instalar el sistema completo se puede seguir las siguientes instrucciones:

1. Descargar imagen de OctoPrint de [este link](https://github.com/OctoPrint/OctoPi-UpToDate/releases/download/0.18.0.op1.7.2/octopi-0.18.0-1.7.2.zip) y flashear en la memoria micro SD
[siguiendo estas instrucciones.](https://octoprint.org/download/)
En [este video](https://www.youtube.com/watch?v=HB_EOfo8OQw) puedes encontrar un tutorial en español.
El software [Balena Etcher](https://www.balena.io/etcher/) es otra alternativa para flashear la imagen.

2. Luego de flashear la imagen en la tarjeta microSD es necesario configurar el sistema operativo para que la raspberry se pueda conectar a la red local de wifi.
  * Conectar la tarjeta microSD con OctoPi flasheado en un computador

  * Busca el archivo 'octopi-wpa-supplicant.txt' y editalo en un editor de texto. No utilices el block de notas o 'notepad' de Windows ya que no cumple las reglas de espacios entre lineas que utilza Linux y puede corromper la instalación, te recomendamos usar [Atom](https://atom.io/), [Sublime Text](https://www.sublimetext.com/), o [Notepad++](https://notepad-plus-plus.org/).

    ![imagen referencial del archivo](img/octopi-wpa-country.png)

  *  Encuentra la sección **## WPA/WPA2 secured** en el archivo y escribe tus credenciales del wifi (**SSID**: nombre de red, **PSK**: contraseña).
  ```
  ## WPA/WPA2 secured
  network={
    ssid="nombre-de-red"
    psk="contrasena"
  }
  ```

  * Encuentra la sección de selección de país y si estas en Chile agrega la línea `country=CL # Chile`. Debería quedar similar a esto:
  ```
  # Uncomment the country your Pi is in to activate Wifi in RaspberryPi 3 B+ and above
  # For full list see: https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2
  country=CL  # Chile
  #country=GB # United Kingdom
  #country=CA # Canada
  #country=DE # Germany
  #country=FR # France
  #country=US # United States
  ```
  * Guarda el archivo, saca la tarjeta de memoria y enciende la Raspberry con el sistema configurado. Ahora deberías poder acceder al sistema por un navegador web en `https:\\octopi.local`

3. Octoprint permite monitorear la impresora 3D con la cámara de la Raspberry Pi u otra webcam de tu elección, si utilizas la cámara de la Raspberry Pi puedes utilizar [este](https://www.thingiverse.com/thing:4634481) brazo impreso en 3D para montar la cámara en la cama de la impresora 3D y poder monitorear tus impresiones o hacer timelapses.

  Tambien puedes imprimir [esta](https://www.thingiverse.com/thing:3016364) carcasa para montar la Raspberry Pi 3B+ en la impresora, o [esta](https://www.thingiverse.com/thing:4586351) si tienes una Raspberry Pi 4.

## PlugIns

#### Discord Remote
  Para monitorear las impresoras desde un canal de discord dedicado en el servidor del FabLab utilizamos el PlugIn [Discord Remote](https://plugins.octoprint.org/plugins/DiscordRemote/).

  La información de configuración del plugin en Octoprint y en Discord la puedes encontrar en [este enlace](https://github.com/cameroncros/OctoPrint-DiscordRemote#setup).

  Para poder enviar comandos desde el bot de discord hacia el OctoPrint debes dar permisos a los usuarios en la configuración de Discord Remote, en la sección de "Access Setings" debes agregar como regla a los usuarios (con su 'user ID') y a los comandos del bot a los que les quieres dar permiso.

 <img src="img/permiso_discord.png" alt="acceso a comandos del bot en discord" width="600">   

 Como ejemplo, en la imagen se puede ver que la primera regla le da permisos a 3 usuarios (separados por comas ',') a que puedan usar todos los comandos (* significa todos), mientras que la segunda regla le otorga a todos los usuarios (*) permisos para usar el comando 'status'.

#### Ngrok Tunnel
  Para conectarse remotamente a la impresora puedes instalar [Ngrok Tunnel](https://plugins.octoprint.org/plugins/ngrok/) que genera una conexión dedicada al servidor de la impresora para conectarte desde cualquier parte del mundo.

  Debes hacerte una cuenta en la página de [Ngrok](https://ngrok.com) y configurar el PlugIn de OctoPrint con tu **Usuario**, **Contraseña** y **Auth Token** de tu cuenta de Ngrok.


### Comandos útiles

Para checkear que el servidor esta corriendo en la raspberry se puede usar el comando:

```
ps -ef | grep -i octoprint | grep -i python
```

Si el servidor esta corriendo debería aparecer algo como esto
```
pi 328 1 0 Aug03 ? 09:57:48 /home/pi/oprint/bin/python3 /home/pi/oprint/bin/octoprint serve --host=127.0.0.1 --port=5000
```

Para reiniciar el servicio de octoprint se puede usar el siguiente comando:
```
sudo service octoprint restart
```

Para revisar la URL pública de ngrok:
```
curl --silent --show-error http://127.0.0.1:4040/api/tunnels | sed -nE 's/.*public_url":"https:..([^"]*).*/\1/p'
```
